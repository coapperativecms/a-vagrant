# Changelog
All notable changes to this project will be documented in this file.

## [0.0.2] - 2017-07-07
Added Vanilla Wordpress
Added Config File:

## [0.0.1] - 2017-06-27
Project environment
Added roots.io bedrock
Created Bash Provision
